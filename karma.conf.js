module.exports = function(config) {
  config.set({
    frameworks: ['jasmine'],

    files: [
      'node_modules/es6-shim/es6-shim.min.js',
      'node_modules/babel-polyfill/browser.js',
      'karma.entry.js'
    ],

    preprocessors: {
      'karma.entry.js': ['webpack', 'sourcemap']
    },

    webpack: require('./webpack/webpack.test'),

    webpackServer: {
      noInfo: true
    },

    reporters: ['dots'],

    logLevel: config.LOG_INFO,

    autoWatch: true,

    singleRun: false,

    browsers: ['PhantomJS'],

    plugins: [
      require('karma-webpack'),
      'karma-jasmine',
      'karma-sourcemap-loader',
      'karma-phantomjs-launcher'
    ]
  });
};