import * as angular from "angular";
import {HttpService} from "./HttpService";


export class HttpServiceImpl implements HttpService {
    public static $inject: Array<string> = ["$http", "$q"];
    constructor(private $http: angular.IHttpService, private Promise: angular.IQService) {}

    public get<T>(url: string, config: angular.IRequestShortcutConfig): angular.IHttpPromise<T> {
        return this.$http.get(url, config);
    }

    public getData<T>(url: string, config: angular.IRequestShortcutConfig): angular.IPromise<T> {
        return new this.Promise((resolve: angular.IQResolveReject<T>, reject: angular.IQResolveReject<T>) => {
            this.get(url, config)
                .success((data: T) => resolve(data))
                .error((exception: any) => reject(exception));
        });
    }

    public delete<T>(url: string, config: angular.IRequestShortcutConfig): angular.IPromise<T> {
        return this.$http.delete(url, config);
    }

    public post<T>(url: string, data: any, config: angular.IRequestShortcutConfig): angular.IPromise<T> {
        return this.$http.post(url, data, config);
    }

    public put<T>(url: string, data: any, config: angular.IRequestShortcutConfig): angular.IPromise<T> {
        return this.$http.put(url, data, config);
    }

    public patch<T>(url: string, data: any, config: angular.IRequestShortcutConfig): angular.IPromise<T> {
        return this.$http.patch(url, data, config);
    }

}