export interface HttpService {
    get<T>(url: string, config?: angular.IRequestShortcutConfig): angular.IPromise<T>;
    getData<T>(url: string, config?: angular.IRequestShortcutConfig): angular.IPromise<T>;
    delete<T>(url: string, config?: angular.IRequestShortcutConfig): angular.IPromise<T>;
    post<T>(url: string, data: any, config?: angular.IRequestShortcutConfig): angular.IPromise<T>;
    put<T>(url: string, data: any, config?: angular.IRequestShortcutConfig): angular.IPromise<T>;
    patch<T>(url: string, data: any, config?: angular.IRequestShortcutConfig): angular.IPromise<T>;
}