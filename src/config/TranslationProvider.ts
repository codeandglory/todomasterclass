export class TranslationProvider {

    public static $inject: Array<string> = ["$translateProvider"];

    constructor($translateProvider : ng.translate.ITranslateProvider) {
        $translateProvider.translations("nl_BE", require("../translations/messages.nl_BE.json"));
        $translateProvider.preferredLanguage("nl_BE");
        $translateProvider.useSanitizeValueStrategy("escape");
    }
}